import { TICKETS, ALTER_TICKETS } from "./constants"
import Storage from '../Storage'


export const saveTicketInReducer = (ticket) => {
    return async dispatch => {
        dispatch({ type: TICKETS, payload: ticket });
    }
}

export const alterTicketInReducer = (idTicket, ticket) => {
    return async dispatch => {
        dispatch({ type: ALTER_TICKETS, payload: { idTicket, ticket } });
    }
}