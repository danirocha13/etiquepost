import { urlBase } from '../config'

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

export const logInUserService = (data) => {
    let url = `${urlBase}/login`
    return fetch(url, {
        method: 'POST',
        headers,
        body: JSON.stringify(data)
    }).then(res => res.json())
        .catch(err => err.json())
}

export const registerUserService = (data) => {
    let url = `${urlBase}/users`
    return fetch(url, {
        method: 'POST',
        headers,
        body: JSON.stringify(data)
    }).then(res => res.json())
        .catch(err => err.json())
}