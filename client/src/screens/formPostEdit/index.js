import React, { Component } from 'react';

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { saveTicketInReducer, alterTicketInReducer } from '../../actions/tickets'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import HeaderBar from '../../components/header-bar';
import './styles.scss';
import Button from '@material-ui/core/Button';
import cep from 'cep-promise'
import Notifier, { openSnackbar } from '../../components/notifier'

class FormPostEdit extends Component {

  constructor(props) {
    super(props)
   
    const index = this.props.match.params.id

    let ticket = this.props.tickets[index]
    this.state = {
      destinatario: ticket.destinatario.destinatario,
      remetente: ticket.remetente.remetente,
      CEPdestinatario: ticket.destinatario.CEPdestinatario,
      CEPremetente: ticket.remetente.CEPremetente,
      logradouroRemetente: ticket.remetente.logradouroRemetente,
      logradouroDestinatario: ticket.destinatario.logradouroDestinatario,
      bairroDestinatario: ticket.destinatario.bairroDestinatario,
      bairroRemetente: ticket.remetente.bairroRemetente,
      cidadeDestinatario: ticket.destinatario.cidadeDestinatario,
      cidadeRemetente: ticket.remetente.cidadeRemetente,
      complementoDestinatario: ticket.destinatario.complementoDestinatario,
      complementoRemetente: ticket.remetente.complementoRemetente,
      estadoDestinatario: ticket.destinatario.estadoDestinatario,
      estadoRemetente: ticket.remetente.estadoRemetente,
      telefoneDestinatario: ticket.destinatario.telefoneDestinatario,
      telefoneRemetente: ticket.remetente.telefoneRemetente
    }
  }

  sendTicket = () => {
    const index = this.props.match.params.id

    let ticket = this.props.tickets[index]

    if (this.state.destinatario == "")
      return openSnackbar({ message: 'Campo destinatário precisa ser preenchido' });
    if (this.state.remetente == "")
      return openSnackbar({ message: 'Campo remetente precisa ser preenchido' });
    if (this.state.CEPdestinatario == "")
      return openSnackbar({ message: 'Campo CEP destinatario precisa ser preenchido' });
    if (this.state.CEPremetente == "")
      return openSnackbar({ message: 'Campo CEP remetente precisa ser preenchido' });
    if (this.state.logradouroRemetente == "")
      return openSnackbar({ message: 'Campo logradouro remetente precisa ser preenchido' });
    if (this.state.logradouroDestinatario == "")
      return openSnackbar({ message: 'Campo logradouro destinatario precisa ser preenchido' });
    if (this.state.bairroRemetente == "")
      return openSnackbar({ message: 'Campo bairro remetente precisa ser preenchido' });
    if (this.state.bairroDestinatario == "")
      return openSnackbar({ message: 'Campo bairro destinatario precisa ser preenchido' });
    if (this.state.cidadeRemetente == "")
      return openSnackbar({ message: 'Campo cidade remetente precisa ser preenchido' });
    if (this.state.cidadeDestinatario == "")
      return openSnackbar({ message: 'Campo cidade destinatario precisa ser preenchido' });
    if (this.state.complementoRemetente == "")
      return openSnackbar({ message: 'Campo complemento remetente precisa ser preenchido' });
    if (this.state.complementoDestinatario == "")
      return openSnackbar({ message: 'Campo complemento destinatario precisa ser preenchido' });
    if (this.state.estadoRemetente == "")
      return openSnackbar({ message: 'Campo estado remetente precisa ser preenchido' });
    if (this.state.estadoDestinatario == "")
      return openSnackbar({ message: 'Campo estado destinatario precisa ser preenchido' });
    if (this.state.telefoneRemetente == "")
      return openSnackbar({ message: 'Campo telefone remetente precisa ser preenchido' });
    if (this.state.telefoneDestinatario == "")
      return openSnackbar({ message: 'Campo telefone destinatario precisa ser preenchido' });

    let destinatario = {
      destinatario: this.state.destinatario,
      CEPdestinatario: this.state.CEPdestinatario,
      logradouroDestinatario: this.state.logradouroDestinatario,
      bairroDestinatario: this.state.bairroDestinatario,
      cidadeDestinatario: this.state.cidadeDestinatario,
      complementoDestinatario: this.state.complementoDestinatario,
      estadoDestinatario: this.state.estadoDestinatario,
      telefoneDestinatario: this.state.telefoneDestinatario
    }

    let remetente = {
      remetente: this.state.remetente,
      CEPremetente: this.state.CEPremetente,
      logradouroRemetente: this.state.logradouroRemetente,
      bairroRemetente: this.state.bairroRemetente,
      cidadeRemetente: this.state.cidadeRemetente,
      complementoRemetente: this.state.complementoRemetente,
      estadoRemetente: this.state.estadoRemetente,
      telefoneRemetente: this.state.telefoneRemetente
    }

    this.props.alterTicketInReducer(index, { destinatario, remetente })
    this.props.history.push('/postlist')
  }

  render() {


    return (
      <React.Fragment>
        <HeaderBar />
        <Notifier />
        <div className="container-login">
          <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="destinatario"
                value={this.state.destinatario}
                onChange={(event) => this.setState({ destinatario: event.target.value })}
                name="destinatario"
                label="Destinário"
                fullWidth
                autoComplete="destinatario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="remetente"
                value={this.state.remetente}
                onChange={(event) => this.setState({ remetente: event.target.value })}
                name="remetente"
                label="Remetente"
                fullWidth
                autoComplete="remetente"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="CEPDestinarario"
                value={this.state.CEPdestinatario}
                type="number"
                onChange={(event) => {
                  this.setState({ CEPdestinatario: event.target.value }, () => {
                    cep(this.state.CEPdestinatario).then(address => {
                      this.setState({ bairroDestinatario: address.neighborhood, cidadeDestinatario: address.city, estadoDestinatario: address.state, logradouroDestinatario: address.street });
                    }).catch(err => {
                      console.log("Cep Não encontrado")
                    })
                  })
                }}
                name="CEPDestinarario"
                label="CEP"
                fullWidth
                autoComplete="CEPDestinarario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="CEPRemetente"
                type="number"
                value={this.state.CEPremetente}
                onChange={(event) => {
                  this.setState({ CEPremetente: event.target.value }, () => {
                    cep(this.state.CEPremetente).then(address => {
                      this.setState({ bairroRemetente: address.neighborhood, cidadeRemetente: address.city, estadoRemetente: address.state, logradouroRemetente: address.street });
                    }).catch(err => {
                      console.log("Cep Não encontrado")
                    })
                  })
                }}
                name="CEPRemetente"
                label="CEP"
                fullWidth
                autoComplete="CEPRemetente"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="logradouroDestinario"
                value={this.state.logradouroDestinatario}
                onChange={(event) => this.setState({ logradouroDestinatario: event.target.value })}
                name="logradouroDestinario"
                label="Logradouro"
                fullWidth
                autoComplete="logradouroDestinario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="logradouroRemetente"
                value={this.state.logradouroRemetente}
                onChange={(event) => this.setState({ logradouroRemetente: event.target.value })}
                name="logradouroRemetente"
                label="Logradouro"
                fullWidth
                autoComplete="logradouroRemetente"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="bairroDestinatario"
                value={this.state.bairroDestinatario}
                onChange={(event) => this.setState({ bairroDestinatario: event.target.value })}
                name="bairroDestinatario"
                label="Bairro"
                fullWidth
                autoComplete="bairroDestinatario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="bairroRemetente"
                value={this.state.bairroRemetente}
                onChange={(event) => this.setState({ bairroRemetente: event.target.value })}
                name="bairroRemetente"
                label="Bairro"
                fullWidth
                autoComplete="bairroRemetente"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="cidadeDestinatario"
                value={this.state.cidadeDestinatario}
                onChange={(event) => this.setState({ cidadeDestinatario: event.target.value })}
                name="cidadeDestinatario"
                label="Cidade"
                fullWidth
                autoComplete="cidadeDestinatario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="cidadeRemetente"
                value={this.state.cidadeRemetente}
                onChange={(event) => this.setState({ cidadeRemetente: event.target.value })}
                name="cidadeRemetente"
                label="Cidade"
                fullWidth
                autoComplete="cidadeRemetente"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="complementoDestinatario"
                value={this.state.complementoDestinatario}
                onChange={(event) => this.setState({ complementoDestinatario: event.target.value })}
                name="complementoDestinatario"
                label="Número"
                fullWidth
                autoComplete="complementoDestinatario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="complementoRemetente"
                value={this.state.complementoRemetente}
                onChange={(event) => this.setState({ complementoRemetente: event.target.value })}
                name="complementoRemetente"
                label="Número"
                fullWidth
                autoComplete="complementoRemetente"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="estadoDestinatario"
                value={this.state.estadoDestinatario}
                onChange={(event) => this.setState({ estadoDestinatario: event.target.value })}
                name="estadoDestinatario"
                label="Estado"
                fullWidth
                autoComplete="estadoDestinatario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="estadoRemetente"
                value={this.state.estadoRemetente}
                onChange={(event) => this.setState({ estadoRemetente: event.target.value })}
                name="estadoRemetente"
                label="Estado"
                fullWidth
                autoComplete="estadoRemetente"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="telefoneDestinatario"
                value={this.state.telefoneDestinatario}
                onChange={(event) => this.setState({ telefoneDestinatario: event.target.value })}
                name="telefoneDestinatario"
                label="Complemento"
                fullWidth
                autoComplete="telefoneDestinatario"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="telefoneRemetente"
                value={this.state.telefoneRemetente}
                onChange={(event) => this.setState({ telefoneRemetente: event.target.value })}
                name="telefoneRemetente"
                label="Complemento"
                fullWidth
                autoComplete="telefoneRemetente"
              />
            </Grid>
            <Grid item xs={12} sm={12} style={{ display: 'flex' }}>
              <Button onClick={() => this.sendTicket()} variant="contained" style={{ margin: '0 auto' }} color="secondary">Gerar</Button>
            </Grid>
          </Grid>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => (
  {
    tickets: state.TicketsReducer.tickets
  }
)


export default withRouter(connect(mapStateToProps, { saveTicketInReducer, alterTicketInReducer })(FormPostEdit));