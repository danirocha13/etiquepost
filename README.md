![image](https://d2my3dgdogz33p.cloudfront.net/logos/colorido/large/603/logo_1489436191.png)

> Etiquepost é o gerador de etiquetas para envio no correio que também possui seu modulo pago para implementação em sistemas!

## Tecnologias

* [ReactJS](https://reactjs.org/)
* [JavaScript/es6](http://github.com/DrkSephy/es6-cheatsheet)
* [SASS](http://github.com/sass/sass)
* [Material-UI](https://material-ui.com/)  Esse é bootstrap que estamos utilizando no projeto.

## Começando

Clone o repositório:

```sh
Primeiro passo clonar o repositório:
git clone https://gitlab.com/danirocha13/etiquepost.git
```

Instalando pacotes npm:

```sh
Na pasta baixada Etiquepost terá 2 pastas Client(Fron-end) e Server(Back-end), para rodar o projeto é necessário 
instalar as dependencias do projeto para isso dentro da pasta Etiquepost execute os seguintes comandos no terminal.

- cd client (Este comando entra na pasta client)
- npm install (Instala as dependencias de projeto do front-end)
- cd .. (Este comando retorna para a pasta Etiquepost)
- cd server (Este comando entra na pasta server)
- npm install (Instala as dependencias de projeto do back-end)

Agora todas as dependencias do projeto foram instaladas. :)
```

## Rodando o prejeto

```sh
Em uma janela do terminal entre na pasta server e execute o comando 
node index.js (Inicia o servidor do back-end)
Em uma nova janela do terminal entre na pasta client e execute o comando
npm start (Inicia o servidor de front-end e abre o navagador)
```

## Project Structure
- Em breve o back-end poderá ser acessado através de uma url então não será mais preciso startar o servidor de back-end.
- Qualquer dúvida podem entrem em contato.
# GG